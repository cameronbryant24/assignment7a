package business;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import beans.Order;
import data.DataAccessInterface;

@ManagedBean
@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative

public class OrdersBusinessService implements OrdersBusinessInterface {
	@Inject
	DataAccessInterface<Order> OrderDataService;

	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;
	
	public List<Order> getOrders() {
		//return all orders from the order data service
		return OrderDataService.findAll();
	}
    public OrdersBusinessService() {

    }

	@Override
	public void setOrders(Order order) {
		// TODO Auto-generated method stub
		
	}
    
    public void test() {
        // TODO Auto-generated method stub
    	System.out.println("Hello from OrdersBusinessService.test");
    }

    
    public void saveOrder(Order order) {
    	
    }
    
    public void sendOrder(Order order) {
    	try {
    		//Get a connection and session to the JMS Connection Factory
    		Connection connection = connectionFactory.createConnection();
    		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    		
    		//Create a message producer for sending messages to the queue
    		MessageProducer messageProducer = session.createProducer(queue);
    		
    		//Create and send a text message
    		TextMessage message1 = session.createTextMessage();
    		message1.setText("This is test message");
    		messageProducer.send(message1);
    		
    		//Create and send an object message (with the order)
    		ObjectMessage message2 = session.createObjectMessage();
    		message2.setObject(order);
    		messageProducer.send(message2);
    		
    		//Clean up by closing the connection to the JMS connection factory
    		connection.close();
    	}
    	catch (JMSException e) {
    		e.printStackTrace();
    	}
    }

}
