package data;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;


import beans.Order;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class OrderDataService implements DataAccessInterface<Order> {

	public OrderDataService() {
		
	}
	
	public List<Order> findAll() {
		
		String url = "jdbc:mysql://localhost:3306/testapp";
		String username = "root";
		String password = "9286148990";
		String sql = "Select * FROM ORDERS";
		Connection conn = null;
		List<Order> orders = new ArrayList<Order>();
		try {
			//connect to the database
			conn = DriverManager.getConnection(url, username, password);
			
			//execute SQL Query and loop over result set
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				orders.add(new Order(rs.getInt("ID"), 
						rs.getString("ORDER_NO"), 
						rs.getString("PRODUCT_NAME"), 
						rs.getFloat("PRICE"), 
						rs.getInt("QUANTITY")));
			}
			
			//cleanup result set
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			//cleanup database
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return orders;
	}

	public Order findById(int id) {
		
		return null;
	}

	public boolean create(Order order) {
		
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/testapp";
		String username = "root";
		String password = "root";
		String sql = String.format("INSERT INTO ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('%s', '%s', %f, %d)", 
				order.getOrderNo(), 
				order.getProductName(), 
				order.getPrice(), 
				order.getQuantity());
		try {
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			//really should check the return value from executeUpdate
			stmt.executeUpdate(sql);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if(conn != null) {
				try {
					conn.close();
				}
				catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public boolean update(Order t) {
		return true;
	}

	public boolean delete(Order t) {
		return true;
	}

}
